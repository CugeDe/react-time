# React-Time

Ultimate time picker for your React app.

* Pick hours, minutes and seconds
* Supports range selection
* Supports virtually any language
* No moment.js needed

## tl;dr
* Install by executing `npm install react-time` or `yarn add react-time`.
* Import by adding `import Time from 'react-time'`.
* Use by adding `<Time />`. Use `onChange` prop for getting new values.

## Getting started

### Compatibility

Your project needs to use React 15.5 or later.

React-Time uses modern web technologies. That's why it's so fast, lightweight and easy to style. This, however, comes at a cost of [supporting only modern browsers](https://caniuse.com/#feat=internationalization).

#### Legacy browsers

If you need to support legacy browsers like Internet Explorer 10, you will need to use [Intl.js](https://github.com/andyearnshaw/Intl.js/) or another Intl polyfill along with React-Time.

### Installation

Add React-Time to your project by executing `npm install react-time` or `yarn add react-time`.

### Usage

Here's an example of basic usage:

```js
import React, { Component } from 'react';
import Time from 'react-time';

class MyApp extends Component {
  state = {
    time: new Date(),
  }

  onChange = time => this.setState({ time })

  render() {
    return (
      <div>
        <Time
          onChange={this.onChange}
          value={this.state.time}
        />
      </div>
    );
  }
}
```

### Custom styling

If you don't want to use default React-Time styling to build upon it, you can import React-Time by using `import Time from 'react-time/dist/entry.nostyle';` instead.

## User guide

### Time picker

Displays a complete, interactive time picker.

## License

The MIT License.