import Time from './Time';
import HourView from './HourView';
import MinuteView from './MinuteView';
import SecondView from './SecondView';

export default Time;

export {
  Time,
  HourView,
  MinuteView,
  SecondView,
};
