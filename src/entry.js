import Time from './Time';
import HourView from './HourView';
import MinuteView from './MinuteView';
import SecondView from './SecondView';

// File is created during build phase and placed in dist directory
// eslint-disable-next-line import/no-unresolved
import './Time.css';

export default Time;

export {
  Time,
  HourView,
  MinuteView,
  SecondView,
};
