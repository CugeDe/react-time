import React from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronUp, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { isValue } from './shared/propTypes';
import { callIfDefined, timesAreDifferent } from './shared/utils';


export default class MinuteView extends React.Component {
  constructor(props) {
    super(props);

    const { value: presetDate } = props;

    this.state = {
      value: new Date(),
    };

    if (presetDate) {
      const { value } = this.state;

      value.setMinutes(presetDate.getMinutes());
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { value: oldTime } = this.state;
    const { value: oldPropsTime } = this.props;
    const { value: stateTime } = nextState;
    const { value: propsTime } = nextProps;
    const shouldUpdate = (timesAreDifferent(oldTime, stateTime)
        || timesAreDifferent(oldTime, propsTime));

    // This will only be called when props are updated
    if (timesAreDifferent(oldPropsTime, propsTime)) {
      this.state.value = propsTime;
    }

    return shouldUpdate;
  }

  onMore() {
    const { onChange } = this.props;
    const { value } = this.state;
    const newValue = new Date(value);

    let minutes = value.getMinutes() + 1;
    let hours = value.getHours();
    if (minutes >= 60) {
      minutes %= 60;
      hours += 1;
      if (hours >= 24) hours %= 24;
    }
    newValue.setHours(hours);
    newValue.setMinutes(minutes);

    callIfDefined(onChange, { hours, minutes });
    this.setState({ value: newValue });
  }

  handleDisabilityMore(event) {
    if (event.keyCode === 13) {
      this.onMore();

      event.preventDefault();
    }
  }

  onLess() {
    const { onChange } = this.props;
    const { value } = this.state;
    const newValue = new Date(value);

    let minutes = value.getMinutes() - 1;
    let hours = value.getHours();
    if (minutes < 0) {
      minutes += 60;
      hours -= 1;
      if (hours < 0) hours += 24;
    }
    newValue.setHours(hours);
    newValue.setMinutes(minutes);

    callIfDefined(onChange, { hours, minutes });
    this.setState({ value: newValue });
  }

  handleDisabilityLess(event) {
    if (event.keyCode === 13) {
      this.onLess();

      event.preventDefault();
    }
  }

  render() {
    const { value } = this.state;

    return (
      <div className="react-time__minute-view">
        <div className="row no-gutters align-item-center">
          <div className="col-12 mt-3 mb-3">
            <button type="button" className="w-100" onClick={this.onMore.bind(this)} onKeyPress={this.handleDisabilityMore.bind(this)}>
              <FontAwesomeIcon size="2x" icon={faChevronUp} />
            </button>
          </div>
          <div className="col-12 mt-3 mb-3">
            {value.getMinutes()}
          </div>
          <div className="col-12 mt-3 mb-3">
            <button type="button" className="w-100" onClick={this.onLess.bind(this)} onKeyPress={this.handleDisabilityLess.bind(this)}>
              <FontAwesomeIcon size="2x" icon={faChevronDown} />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

MinuteView.propTypes = {
  onChange: PropTypes.func,
  value: isValue,
};
