import React from 'react';
import PropTypes from 'prop-types';
import { polyfill } from 'react-lifecycles-compat';
import mergeClassNames from 'merge-class-names';

import HourView from './HourView';
import MinuteView from './MinuteView';
import SecondView from './SecondView';

import { isClassName, isValue } from './shared/propTypes';

import { callIfDefined, timesAreDifferent } from './shared/utils';

export default class Time extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: new Date(),
    };

    if (props.value) {
      this.state.value = props.value;
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { value: oldTime } = this.state;
    const { value: oldPropsTime } = this.props;
    const { value: stateTime } = nextState;
    const { value: propsTime } = nextProps;
    const shouldUpdate = (timesAreDifferent(oldTime, stateTime)
            || timesAreDifferent(oldTime, propsTime));

    // This will only be called when props are updated
    if (timesAreDifferent(oldPropsTime, propsTime)) {
      this.state.value = propsTime;
    }

    return shouldUpdate;
  }

  /**
     * Event
     */
  onChange = (values) => {
    const { value: oldValue } = this.state;
    const value = new Date(oldValue);
    const { onChange, secondsEnabled } = this.props;
    const { hours, minutes, seconds } = values;

    value.setHours(hours !== undefined ? hours : oldValue.getHours());
    value.setMinutes(minutes !== undefined ? minutes : oldValue.getMinutes());

    if (secondsEnabled) {
      value.setSeconds(seconds !== undefined ? seconds : oldValue.getSeconds());
    } else {
      value.setSeconds(0);
    }

    callIfDefined(onChange, value);
    this.setState({ value });
  }

  /**
     * Render time selection component
     */
  renderContent() {
    const {
      secondsEnabled,
    } = this.props;

    const { value } = this.state;

    return (
      <div className="d-flex align-items-center">
        <div className="ml-auto mr-auto">
          <HourView
            onChange={this.onChange}
            value={value}
          />
        </div>
        <div>
                    :
        </div>
        <div className="ml-auto mr-auto">
          <MinuteView
            onChange={this.onChange}
            value={value}
          />
        </div>
        { secondsEnabled
                    && (
                    <div>
                        :
                    </div>
                    )
                }
        { secondsEnabled
                    && (
                    <div className="ml-auto mr-auto">
                      <SecondView
                        onChange={this.onChange}
                        value={value}
                      />
                    </div>
                    )
                }
      </div>
    );
  }

  render() {
    const { className } = this.props;

    return (
      <div
        className={mergeClassNames(
          'react-time',
          className,
        )}
      >
        { this.renderContent() }
      </div>
    );
  }
}

Time.propTypes = {
  secondsEnabled: PropTypes.bool,
  className: isClassName,
  onChange: PropTypes.func,
  value: isValue,
};

polyfill(Time);
