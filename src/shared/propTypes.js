import PropTypes from 'prop-types';

export const isValue = PropTypes.instanceOf(Date);

export const isClassName = PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.arrayOf(PropTypes.string),
]);
