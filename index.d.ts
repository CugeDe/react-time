declare module "react-time" {
  type TimeCallback = (time: Date) => void
  type OnChangeTimeCallback = (time: Date | Date[]) => void

  export default function Time(props: TimeProps): JSX.Element;

  export interface TimeProps {
    className?: string | string[];
    onChange?: OnChangeTimeCallback;
    value?: Date | Date[];
  }

  export interface TimeTileProperties {
    time: Date;
  }

  export function HourView(props: TimeProps): JSX.Element;
  export function MinuteView(props: TimeProps): JSX.Element;
  export function SecondView(props: TimeProps): JSX.Element;
}